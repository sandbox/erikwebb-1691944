<?php

/**
 * Set a minimum Drush version on which Drush multiply will run.
 */
define('DRUSH_MULTIPLY_MIN_VERSION', '5.0');

define('DRUSH_MULTIPLY_SETTINGS_HEADER', "

////////////////////////////////////////////////////////////////////////////////
// The following settings have been created by Drush Multiply.                //
////////////////////////////////////////////////////////////////////////////////

");

/**
 * Implementation of hook_drush_command().
 */
function multiply_drush_command() {
  // The 'multiply' command
  $items['multiply'] = array(
    'description' => 'Create multiple, identical sites within a single docroot. This is designed for classroom-based training, where many sites need to be created at once.',
    'arguments' => array(
      'base-site' => 'The site name of the master site.',
      'target-site(s)' => 'A comma-separated list of site name(s) for the clone(s).',
    ),
    'required-arguments' => 2,
    'options' => array(
      'domain' => array(
        'description' => 'The domain suffix to be added to cloned sites.',
        'example-value' => 'example.com',
      ),
    ),
    'examples' => array(
      'drush multiply base clone1,clone2 --domain=example.com' => 'Create two clones of site "base," named clone1.example.com and clone2.example.com.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  return $items;
}

/**
 * Implementation of drush_hook_COMMAND_validate().
 *
 * The validate command checks to ensure that the master site exists and the
 * clone sites do not.
 */
function drush_multiply_validate($base, $clones) {
  // Make sure Drush meets the minimum version requirement.
  if (!version_compare(DRUSH_MULTIPLY_MIN_VERSION, DRUSH_VERSION, '<')) {
    return drush_set_error('MULTIPLY_DRUSH_VERSION', dt("The multiply command requires Drush version >= !drush_version.", array('!drush_version' => DRUSH_MULTIPLY_MIN_VERSION)));
  }

  // Check that the base site currently exists.
  if (!drush_sitealias_get_record($base)) {
    return drush_set_error('MULTIPLY_BASE_MISSING', dt("The base site '!base_site' does not exist.", array('!base_site' => $base)));    
  }
  
  // Check that each of the clones does _not_ exist.
  foreach (explode(',', $clones) as $clone) {
    if (drush_sitealias_get_record($clone)) {
      return drush_set_error('MULTIPLY_CLONE_EXISTS', dt("The clone site '!clone_site' already exists.", array('!clone_site' => $clone)));   
    }
  }
}

/**
 * Where the magic happens.
 */
function drush_multiply($base, $clones) {
  $clones = explode(',', $clones);

  $base_alias = drush_sitealias_get_record($base);
  $base_conf_path = drush_conf_path($base);

  $domain = drush_get_option('domain');
  // If the user did not set a domain, use the FQDN of the master site as the
  // suffix.
  if ($domain == NULL) {

  }

  // MULTIPLY!
  foreach ($clones as $clone) {
    // Copy all files from base site to clones.
    drush_log(dt("Copying '!base' site to '!clone'.", array('!base' => $base, '!clone' => $clone)), 'notice');
    drush_copy_dir(drush_conf_path($base), drush_get_context('DRUSH_SELECTED_DRUPAL_ROOT') . "/sites/$clone");

    $clone_conf_path = drush_conf_path($clone);

    // Set default database to include a prefix.
    $clone_alias = drush_sitealias_get_record($clone);
    drush_sitealias_add_db_settings($clone_alias);
    $clone_alias['databases']['default']['default']['prefix'] = "${clone}_";
    
    // Add customized settings to bottom of new settings.php file.
    $fp = fopen($clone_conf_path . '/settings.php', 'a');
    $settings = DRUSH_MULTIPLY_SETTINGS_HEADER;
    $settings .= '$databases = ' . var_export($clone_alias['databases'], TRUE) . ";\n";
    $settings .= '$base_url = "http://' . $clone . '.' . $domain . "/\";\n";
    if ($fp && fwrite($fp, $settings) === FALSE) {
      drush_log(dt("Unable to update settings for site '!clone'.", array('!clone' => $clone)));
    }

    // Sync databases.
    drush_invoke('sql-sync', array($base, $clone));
  }
}
